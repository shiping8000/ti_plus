package com.qiangou.ti.dome;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Test;

import com.qiangou.ti.util.JsonUtils;
import com.qiangou.ti.vo.CartVo;

/**
 * 加到购物车模拟
 * @author Administrator
 *
 */
public class CheckCart {
	
	
	@Test
	public void testCart() throws IOException {
		System.out.println("-------  testCart   请求来了~~~    -------");
		
	
		// 请求地址
		String url = "https://www.ti.com.cn/occservices/v2/ti/addtocart" ;
		HttpClient client = new HttpClient();
		
		// post请求方式
		PostMethod postMethod = new PostMethod(url);
		
		CartVo cart = new CartVo();
		cart.setBatchCode("");
		cart.setDienCode("");
		cart.setOpnId("AMC1336EVM");
		cart.setPackageOption(null);
		cart.setPcrCode("");
		cart.setQuantity("1");
		cart.setSparam("");
		cart.setTiAddtoCartSource("ti.com-productfolder");
		cart.setWeek("");
		cart.setYear("");
		
		List<CartVo> cartList = new ArrayList<CartVo>();
		cartList.add(cart);
		System.out.println("cartRequestList:"+JsonUtils.objectToJson(cartList));
		// 推荐的数据存储方式,类似key-value形式
		NameValuePair telPair = new NameValuePair("cartRequestList", JsonUtils.objectToJson(cartList));
		NameValuePair pwdPair = new NameValuePair("currency", "CNY");
		// 封装请求参数
		postMethod.setRequestBody(new NameValuePair[] { telPair, pwdPair });
		
		// 这里是设置请求内容为json格式,根据站点的格式决定
		// 因为这个网站会将账号密码转为json格式,所以需要这一步
		postMethod.setRequestHeader("content-type", "application/json");
		
		postMethod.setRequestHeader("cookie", "ti_ua=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4662.6 Safari/537.36; user_pref_language=\"zh-CN\"; CONSENTMGR=ts:1638344407862|consent:true; _ga=GA1.3.666666184.1638344408; _gcl_au=1.1.503510175.1638344410; _fbp=fb.2.1638344410324.289473467; __adroll_fpc=4c2200562bfaf1cd781880af5863e15b-1638344410975; ELOQUA=GUID=5D3E7DAEE7984587B552787F903E7B2D; tiSessionID=017d74f1193d0001e1f4adef79f505071012506900bd0; login-check=null; login-check=null; user_pref_shipTo=\"CN\"; user_pref_currency=\"CNY\"; coveo_visitorId=606b89f2-3603-4408-9e28-6ba52656f544; ti_bm=; _gid=GA1.3.31299235.1639392521; ti_geo=country=CN|city=GUANGZHOU|continent=AS|tc_ip=116.24.66.223; last-domain=www.ti.com.cn; user_pref_givenNameLocalLanguage=\"泽凯\"; alias=homepageproduct; ti_get_lit=ts=1639554461806|lit=bq76930|pid=|aid=|gpn=Non-Product|pt=search|evt=20211115154741806|; __ar_v4=G3YHLXUICZC3XKDXYVVLO4:20220013:5|2XNKMR6P4VGD5MD3ZP4SQR:20220013:5|QFXRHQEHOJDMLHSLFIWCLO:20220013:5; userType=Anonymous; _gat_ga_main_tracker=1; auth_session=CwwS55CSa5DoGVLd.TO7VpzyQEmoVH3Zmriy_HCwxO5bvofbQxWmDH6GzQnFdDiH0djQQibDY9l9tIQHHxkzlRlh6doaH0CdNjynf1Nxx9qBdwvzZbN7fzQwJgjRo3cP2i9pqHbblUlYXtTPmYaQ69lntk94KxTA-KIXEgTu79jR3A8_crZEwWJMKvvpKM4xJrhe_fQgryMc0iWrV2YhTlzZaA0EPPzkZUkszzOJI-qKPg-S3lHWE1rZQ9dbGsBrudxXKjVn7CYU02Xcbnxmlb5Zx6_oi-xvv5hkLR4cAnELbhemV3BLX4hkdECSL0JkWBgOeKL0ZMEApyhsqKKt7gczXSZrTn_hVPExeAK9qgBrlCMq8NWxhxohYyGY5x-Psx6JYrWJxCsoPi6pHgbBgftoaQ3jwKD5EC33oyuh3shC6qu5HM6Kmyk_AYROJuEwl2fDJINZnqxjMd7UHIowu3vB3G_DMtVfJJCDvUEig9yNevtTKdMB-h8OyXGbGmWYW1bmlbJBdM2nUzfZ9C3X6KX2hweFTkNjqZwSdxaGuwkOS2HJky79dtphLyU_0L9rryuAg0jS84uesm5UMyZ5Ek6M8xOE0QtHNEqkRYvXC8aCcUJ1gXKy5EbJ0Y-V3J6TjetUwl_5YGjNjFb-qGPnlBareDw5--TL_QhQhiV19xxXycYOn2F_76StnPSkdkiZRTB563dFzDTc6b_nJ2PjEJrpQMZjQoSZ72RJilz7bt90HfkL1-WukHQuxp2JtLn_ULOdIQwH6RGAdqoymxAZOKWEityk5JoOdBtBx-cTcuczO-VMNubC3ptoe3uIm5JHvCF-laeFXKgN6qPXYimqW53yqOTW35Mu1tMZYv3AowibyZYKQHTb37zYad_b-Jj-t7leybsOu8ZcmlCi29KdjruKs8ywrXS7CM3qpHuQ4CYmRCKCaeEukikL3ztA9Xv3a0A6Q_JYF54viSld3gXEyDUUqs3AubfpFAAZiMtb9nOF3iAZKx5_0OpYzUsYoim6G48KTeyxwAUiRwMI2HKNFYIWe6TyzG_PGaqpiLoyQOoeNWzJIZnYS1mPT8jweYIW6CGvovjy2mJNnaniEmAwz4szlbV676vD7oZJRD3wMMvAQlccCNTW3RANtGAT1d6Q3DYHp5YRA3-75zx5-GiRN5aG5rJx501YfIgrlKUCYj6Ge5EQ7LfFYHokapZxQisW7RziwNpC20-m589jQBBDgKwZYfeD_n2CgJin2cJABdGr6yE0pr5PWV7BhTlGg9u_UcamJZTBSxsKbaquoHY60OkTtybB7diZMPGNk79maG68tMamH5vk4YOJhXnrf22X6YXT1A3Btcr7la-AHHVRbVOpDmTMGtUQrdfz_8DIQ8KxtTqUUra5vnZrmXcBEvcN7Uv0mieOVDKpI4mJPaincHggXP2g1XHalguEr8sgnqho1e6d5C0_Qc_8MR-_68AjFxuB2lYFwrlbRrG-u70J5tIhmtwhPLFQzDm7C0VizbFRaTEH6O6aIMf3NTus0_ELAdNXzo0yxGn9IKeWDN-mBGhkWrz7jmhWRI0LCvuRdUl37YxIWvPZw6mViJe3Ipd2CpacnoXg31qau_QcAhLja_5vGEfy6YFHymOU_ZXUt2eGr9Q-9DLOUHQfNX8Xww9MeX43Z8PLEBeZCfqm7O9gWwLJKIQIkuYPcq26msFfbfH-uJpL2MOh496uO6cJaAKpdhQxmY8yP-KWBoiZsZ992zaCWNvuNPQKpOtZMof_65c6qzBKb1GogOv-w44WMAq_IXgaRWWeEnzj33K8CdNN1-lnj3216YiPAcBP3puH0LZdCOVGVOw4rFYRjPcRyzYU590XCdF3eo8fVpLpnx70Dg0yHMR4Am8yNLVlXIKEbnmYZICj1IR9bfUbFrnOwAfGUOouWh6zYdBsbv18Sw2PE29kEbidfHM76iLfZ5E7iTJZMGMwwMtRJ0W2YUtifHJacnNQPYMENvHpU3Atn759LmOLN_oXMksvMVxBCQvnVzxQz0_fPBWgXcxrRgWioAHUS8nz8YynDS9ZmCAGkL7BZl6WV1MpZgL3su38C3ni7l9a_E5CMIAmQBMOxPba3Se8l51cXtN8esYwDBdBbTn-1b2mFMTRDRuBRVtxTOTe9NtmTTU6BvuEqvRkBD_J8KRTOh1MrIUoav-hkfW7yS6pvq6LLyIquoqk.u2G6l03xKsQAf6SlstJbvg; bm_sz=65E78030EEE640010175FE2DCEAFA17B~YAAQe0InOwkcpot9AQAASoTLvQ5SWI1o7hriBHfvmoxlDPAPo5SiJyu04s+x9h+9CsBmPR3mfcuuu2j3ydLgF3xugJfy/A+8QsdEg6gNrlsUZG/2ewZ2gd0RIU2Ngn87OD/8ivlotxq7394ucRPOkXA6kzqfMaPxPSl5qSrGjJVESHe7MTMoPDXJEL2eqXuFYvPDnRA7T4PDFNeBRrTyh5D29kTtE2zT6HRfM/mfQfBlABbnueAsssQQl8eHLzEKBn9cQZAbdyyzEI+NQxiYCfTqJXVZKvKAUonN0eDyh6Rgd3kEksPsKdJriDRu19dMG2lP233UPV5YdYIC5ik3gXFE1bas9YmNodhA2B+FD5/JOIikQCQjF4otOxcn9PfrlmFwCcI9TjHVr2YgWlk=~3424825~3748401; bm_mi=64E46A5BD9B8A50B63ED8C325B2E299E~F+Tjmo6lysdZ6w6H7lzy7o9S3TRNSlXjKIyksxvXltPmcG7TqMRwYVB9+2RCsG+rah7gdbqbvD+2BkEmxSFSFL/n5X4p0wJe+5yaQQuZKvokzdAmKeM3M79bMzel+YWYDVUEegh9nIbjAb4XH1zm53LOqzWfziKsaElEYQjCIbaSjhn+38VLvDAg1Ku8w0QVTD5iO/1C8hDWUK0KVljI+4c58ZsuSQlAi8Mpyh+EYLqmS6SjBsq9uCzeSDLmaYeQWgPe/Grfs3V7AYSmPB0mAH+aJIevEvYoAw+H/Pf+SnILBq/zTgrp4PqWmnelEKz/; user_pref_permanentId=6917553; user_pref_givenName=; user_pref_uid=gdzk168@outlook.com; acceleratorSecureGUID=376f393fcc5537205bef2127b48a18ccec9d2c7b; gpn=Non-Product; ak_bmsc=44569DB908B5BFC0E15ACF5241D7A3F2~000000000000000000000000000000~YAAQe0InO6slpot9AQAARNPLvQ5ZpLiasLu0e50tDjwqt0YH1erpfPzUcf+U1U191xh6+sqwLYH09fJ2kgcTTlpvsig1Z6FgnMH/+fRB0EYRP+/LraaadepLkPwqzixMKRPuJnjtxN0LO1fLylsM0k3wZJtd8RFobORz+nbQCTMiANGrT/px4sgy7a3K0g3JWIoRx56rgPQMNok5SOovP1/NSNl1+83r6VuRIaGvES8F207HR0Eg+FFGfWDj1+mTmsmtORdy8Nwc6BtdbFAZDAew1fduGVU0bSugkuW4OvqQRZ32oSeLAgF3yW5xP1PTEmMPIVi+hirMY7XjTZj5bMLgO8/2ZupFiHE2cNh74YkqtsrzVhKFUo+jGVhmdPdV4LjfCSC1+n6pDIQMpbwSYYZnBCxASGefIc3VQJGPrjm3ocHAhjqVzZG5TXCz3KWbN0uBBd31JBefZ9xC58tX0Au6ja5CcxC2hz+tQkkIUzEK0R6wdY4U9YU//jtaS+6fTY4skgDWK+M023+WW+JERQ==; tipage=ti reference designs/ti reference designs library cn; tipageshort=ti reference designs library cn; ticontent=ti reference designs; ABTasty=uid=vypwvnt3c01qrsxa&fst=1639485229472&pst=1639553042804&cst=1639566780445&ns=3&pvt=26&pvis=3&th=686831.851794.9.2.3.1.1639485250594.1639566792679.1; ABTastySession=mrasn=&sen=6&lp=https%3A%2F%2Fwww.ti.com.cn%2Fstore%2Fti%2Fzh%2Fcart%3FcontShopUrl%3Dhttps%3A%2F%2Fwww.ti.com.cn%2Fproduct%2Fcn%2FSN6501%23order-quality%26_ticdt%3DMTYzOTU2Njc1NXwwMTdkNzRmMTE5M2QwMDAxZTFmNGFkZWY3OWY1MDUwNzEwMTI1MDY5MDBiZDB8R0ExLjMuNjY2NjY2MTg0LjE2MzgzNDQ0MDg; da_sid=2EBC23818E32AE89DF07AA1349EBB3B883|4|0|3; da_lid=1D8F10B29A73EA12815BBB990B20DDA568|0|0|0; da_intState=; bm_sv=E79C4ACA66E5A23354B1486C1927209B~/g/vUgbG4OK1AWB00ECd3P4Bi9Th0YMESsWPF2YgRashCG11PaE+HLA10BhVlRunvH6jU0QaCeJN5+4WHtvkiqzMq0lew68eGb82nKmCXdhHXP0mdG98mMkJjh1OGrOahViaEZxDzyA/DLdFLpqHqRKqQKXTAVT3w45WMzkZ/WI=; ga_page_cookie=product page AMC1336EVM zh; ga_content_cookie=/tistore; ti_rid=2650084b; _abck=96110CECF7C7315B77F0E0EF27EAB96A~-1~YAAQe0InOy8tpot9AQAAGBPMvQdQClvWUiByxHpKeefrbNUYUUhFjyMAaH9qyNVZCjny5mZVpdxY02MMiR/G19ciaWrfj3ppTmhN8a696UmMzbKqsDHOtIaNccWyVpK0zumDeRcxycAzRlQye1kd6bkXjbNgZLw3vE5Ppgrj22dRmyCYPD1d9PM4Qoj5OBIFWR1Xhujcq4p9hrv1XVtc1V0wFqBcEfTwT1XWzvghku3DA/eso9Cgui4nHlmmMsrX3AGSA43OOH9T1U0oGtl56mfA7To5yNcYIV3Zvocv+jv0yWTC9F9blHYChsx0flQD61DxGLdWuauosiV4W5kGogevXf9dEy9Wh7fmxfYDEJ4oFcs2WHv+gpsV5HVWngG3cUbmcbEL36lO4HLo3XntB85KbLG90rWrWC2Trk/3e2c7SrvOjmVySLNL7nfLhJSh9MsKyQMz7P6UXKQjukLMsQ1WN+tJMYz6Ofv0LCsOYwWaQw==~-1~-1~-1; utag_main=free_trial:false$_st:1639568609925$v_id:017db8ef47bc007120575ddec7a805071005206900bd0$_sn:3$_ss:0$dc_visit:3$_pn:3;exp-session$ses_id:1639566780183;exp-session$dc_event:2;exp-session$dc_region:ap-east-1;exp-session");
		
		postMethod.setRequestHeader(":authority", "www.ti.com.cn");
	  /*postMethod.setRequestHeader(":method", "POST");
		postMethod.setRequestHeader(":path", "/occservices/v2/ti/addtocart");
		postMethod.setRequestHeader(":scheme", "https");*/
		postMethod.setRequestHeader(":method", "POST");
		postMethod.setRequestHeader(":path", "/occservices/v2/ti/addtocart");
		postMethod.setRequestHeader(":scheme", "https");
		postMethod.setRequestHeader("accept", "*/*");
		postMethod.setRequestHeader("accept-encoding", "gzip, deflate, br");
		postMethod.setRequestHeader("accept-language", "zh-CN,zh;q=0.9");
		postMethod.setRequestHeader("cache-control", "no-store, must-revalidate");
		postMethod.setRequestHeader("content-length", "141");  //变化值
		postMethod.setRequestHeader("expires", "0");
		postMethod.setRequestHeader("origin", "https://www.ti.com.cn");
		// https://www.ti.com.cn/store/ti/zh/p/product/?p=AMC1336EVM   变化值
		postMethod.setRequestHeader("referer", "https://www.ti.com.cn/product/cn/SN6501");
		
		postMethod.setRequestHeader("sec-ch-ua", "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"");
		postMethod.setRequestHeader("sec-ch-ua-mobile", "?0");
		postMethod.setRequestHeader("sec-ch-ua-platform", "\"Windows\"");
		postMethod.setRequestHeader("sec-fetch-dest", "empty");
		postMethod.setRequestHeader("sec-fetch-mode", "cors");
		postMethod.setRequestHeader("sec-fetch-site", "same-origin");
		postMethod.setRequestHeader("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4662.6 Safari/537.36");
		postMethod.setRequestHeader("x-sec-clge-req-type", "ajax");
		
		
		// 可不传变量
		//postMethod.setRequestHeader("newrelic", "eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjE3MjA1OTQiLCJhcCI6IjExODYyMDk5ODQiLCJpZCI6IjcxMTlhMWYyZmY4MWIyMTQiLCJ0ciI6ImVhZjBmOWQ1Y2FmZmQwYWY1YTYzZWVlZGY5MzJiZDYwIiwidGkiOjE2Mzk1NTUxMjg3NTYsInRrIjoiMTU2NTEzNiJ9fQ==");
		//postMethod.setRequestHeader("traceparent", "00-eaf0f9d5caffd0af5a63eeedf932bd60-7119a1f2ff81b214-01");
		//postMethod.setRequestHeader("tracestate", "1565136@nr=0-1-1720594-1186209984-7119a1f2ff81b214----1639555128756");
		
		
		System.out.println("执行请求");
		// 执行请求
		client.executeMethod(postMethod);
		System.out.println("执行请求w");
		
		// 通过Post/GetMethod对象获取响应头信息
		String cookie = postMethod.getResponseHeader("Set-Cookie").getValue();
		// 截取需要的内容
		System.out.println("返回："+postMethod.getResponseBodyAsString());
		
		
	}
	

}
