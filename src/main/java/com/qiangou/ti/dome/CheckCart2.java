package com.qiangou.ti.dome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.qiangou.ti.util.JsonUtils;
import com.qiangou.ti.vo.CartVo;

/**
 *  加到购物车模拟二
 * @author Administrator
 *
 */
public class CheckCart2 {
	
	
	
	public static void main(String[] args) {
		
		System.out.println("-------  testCart   请求来了  -------");
		// 请求地址
		String url = "https://www.ti.com.cn/occservices/v2/ti/addtocart" ;
		// post请求方式
		CartVo cart = new CartVo();
		cart.setBatchCode("");
		cart.setDienCode("");
		cart.setOpnId("AMC1336EVM");
		cart.setPackageOption(null);
		cart.setPcrCode("");
		cart.setQuantity("1");
		cart.setSparam("");
		cart.setTiAddtoCartSource("ti.com-productfolder");
		cart.setWeek("");
		cart.setYear("");
		
		List<CartVo> cartList = new ArrayList<CartVo>();
		cartList.add(cart);
		System.out.println(JsonUtils.objectToJson(cartList));
		
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("cartRequestList", JsonUtils.objectToJson(cartList));
		parameters.put("currency", "CNY");
		
		String result = sendPost( url,  parameters);
		
		System.out.println(result);
	}
	
	
	
	
	 /** 
     * 发送POST请求 
     *  
     * @param url 
     *            目的地址 
     * @param parameters 
     *            请求参数，Map类型。 
     * @return 远程响应结果 
     */  
    public static String sendPost(String url, Map<String, String> parameters) {
        String result = "";// 返回的结果
        BufferedReader in = null;// 读取响应输入流
        PrintWriter out = null;
        StringBuffer sb = new StringBuffer();// 处理请求参数
        String params = "";// 编码之后的参数
        System.out.println("请求地址是:"+url);
        try {  
        	if(parameters.size()>0){
            // 编码请求参数  
            if (parameters.size() == 1) {  
                for (String name : parameters.keySet()) {
                    sb.append(name).append("=").append(  
                            java.net.URLEncoder.encode(parameters.get(name),  
                                    "UTF-8"));  
                }  
                params = sb.toString();  
            } else {  
                for (String name : parameters.keySet()) {
                    sb.append(name).append("=").append(  
                            java.net.URLEncoder.encode(parameters.get(name),  
                                    "UTF-8")).append("&");  
                }  
                String temp_params = sb.toString();
                params = temp_params.substring(0, temp_params.length() - 1);  
            }  
        	}
            System.out.println("发送的请求参数是:"+params);
            // 创建URL对象  
            java.net.URL connURL = new java.net.URL(url);  
            // 打开URL连接  
            java.net.HttpURLConnection httpConn = (java.net.HttpURLConnection) connURL  
                    .openConnection();  
            // 设置通用属性  
            httpConn.setRequestProperty("Accept", "*/*");  
            httpConn.setRequestProperty("Connection", "Keep-Alive");  
            httpConn.setRequestProperty("User-Agent",  
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");  
           //httpConn.setRequestProperty("Content-Type","application/json");//设定 请求格式 json，也可以设定xml格式的
            //httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            //传入
            httpConn.setRequestProperty("content-type", "application/json");
    		httpConn.setRequestProperty("cookie", "ti_ua=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4662.6 Safari/537.36; user_pref_language=\"zh-CN\"; CONSENTMGR=ts:1638344407862|consent:true; _ga=GA1.3.666666184.1638344408; _gcl_au=1.1.503510175.1638344410; _fbp=fb.2.1638344410324.289473467; __adroll_fpc=4c2200562bfaf1cd781880af5863e15b-1638344410975; ELOQUA=GUID=5D3E7DAEE7984587B552787F903E7B2D; tiSessionID=017d74f1193d0001e1f4adef79f505071012506900bd0; login-check=null; login-check=null; user_pref_shipTo=\"CN\"; user_pref_currency=\"CNY\"; coveo_visitorId=606b89f2-3603-4408-9e28-6ba52656f544; ti_bm=; _gid=GA1.3.31299235.1639392521; ti_geo=country=CN|city=GUANGZHOU|continent=AS|tc_ip=116.24.66.223; auth_state_BLcz4-83CK0GEDvHeBpHdVf39eg=daNoMRt0XOIqvEqG.y5NBgTV180NPzGYsay1txJt1vD_-0vLLufVVBxSbAlJ9WTq7KC9RegrvOgdF6S29Kmfij_P1eUjIR1Tz2mvRZ5hRZQjf9OVpIS3mLKEiom93jF0aq_UytlMhWpJu-XQu6EiHHIwObAtAiZ309Fc8pER4oZ-ltIW29KC_0Cn0MTDBKzaXCQQxH64Jb4eo9ErgKxMQ2RfwDY-0GfZSpL1kINyG9B3PrwvTZsZAuqBXqX1-iI8vuMtT6NSrJsHoq8a7c02rXP4OiqOr8UZGjbbpB0JhDskbiYh5EsdDjqvdKywLCs0yHK4HVKpYkLAWkCaMOsslcEi79dRCas96tLwOwBz_kM_obq77vIcWzkciuEyssfBpKXx0q9yElJTV38lQTN1qNY1aUDdrUywNlno.aRSV_UqO7GI9BDirrtgc1w; last-domain=www.ti.com.cn; auth_state_MzyOb1Jj_1GmHB6cZyPyy2bm_5c=9RpEg7f-hIzJAJE_.RJ69Qc-ShCPriieZKx0KkB3TfxXMekhd9OeWdU3ircb9j8wjWc_x_wbyiZh9uPy6Gqe77V2Pn6pBNzEgmZAnLAOlO7PegXFNZtctpyOjyk7wb7emc-HhpTnsbY7NS8tis1YlWyJxAY_lK1NEqfDttQEGCyP8Zv7IVKsC9RSHeT4WGb7KpRrvtt-6FBTSG5xZ8JTZ9bOL1RZHTEhLXjfcmR3i7nbyUDHafE97iSRECP_ESiVJdJkkLLyltMrUpKl-rco5JFxyFF1beQNchjEUIBN1tOEMMB4o6ZnvWeeUpEyZEcgxwOGwdM5_5xEB_XTtr4mAX8SUVKAnZsbqrYHvQdJNFvuJXyd4oEPehT5_E405OqC9sL8aduyAcs6YR1jHqZdopekNNCGV-QX5apThuxnh9XyHQB5Z7AyzjrT16HCdXLNuhKp_x9A_OINp-7umHX75tRIk0DI4MkTjUVYpkMM8.Ev8on9xgjGThNa95K4VGPQ; auth_state_YpYSHSFq9rpyBFTKd-UShJHlo3g=JjkUfEaBNOCHWGyu.MabTCWy-Kr8S3QBAoDaMtQEiVhh-GBiyc_XHfAr14hgpYA12Zbgd5xQJCdvVRlvcMQlNYc7_z6ZnUGzg5vnVueXSivIFXPRFMAUhmYIuakFE8ywqnNZAhxy385MFQnJI0oVjQElp5V3bYhZX7zZNMFxR3jv0XoDpyR5on94in0qLOLCU1_QZXjZz-pGcpI_c9yQUeDjxNw_sN9xijueuUotJzy2w8q5pSjPDtjU-T_D_CdTHouSCkIRvbMPei5sRplL7PtxPW3qVAOLiApI1rzfdY1sA9VmVu6sXMP7lywrnkg06Z09p2Cl9koeW50Fgu-HI8bQgdxXB1P7F8kqGrb8DaJRI9rUveEAq_jCO-vHGGmUdL4-ypMPzERZjbq8lXPDcbYuTAdNIhzslf__nM4wg5Ktm8kdYrEcqzLE3xtA8NXq8c5mwwxUnkqaimQl0XcIqqT26GOK5xp_kBOAXtn_o.w5P3qX2zQztXA-V0Ze8tlg; auth_state_bbrutxvdceciytFs8ye2If2uTcc=LUBqjN1lDR3SJMtY.Tt4eDVUZscs5PzDpnUW7WdcpxJTHotyAN5ocaGpHokL4liTq8sMe3fn-qssbNL032R9VAKnkbsulGDr_J3MYC5E6yrKSq8M8J0JM2JBOPiUiL8_Br-d_qLlU3R4805cVbwT14jbNhiFyvzSFPVIG_EEHgzqXDDenttlSTnTX-Ls7wcU8uWYMCwJ0ff52lUoknx8YpFt5l1tUptMCdXPRDvWekLubT60NkP_01BsGd9Ls4GrhYrPdpHpImzyP7CPjXuQaW78ds83fpgrE1vrIAFlr1583J62BBFBqIPgHQFzfbkmX1IfkZUDs967_WwzfWai0xV1qI5C9zseeoKj-3SxeZNBCPQk3hhYCaVQidCLts42y6LUeQPByioEeM6EekrITeeJzmYq8XhAqy-Tp3iDpksZMAJySZt7XfwQ72t4MN916qvuaM0CtLslTJU32RnmHpmyyw8BTmZKQkaGYMD58.X86y3tqufgbfWZs0uCx6MA; bm_mi=4058E51E1D6F526901B7AFD0F41D22E1~JAGTq/3/6RRM2S6I7IIUjI/xHRGjOx4JHCKCE/Ekf6E7yvxtCiRgg1LSehN2ZB8hf+ZVWVxhtEnh8d5r5v+8edmSbuVBAjgfZx1aagVdTjD25qSWX/+EwuMf+DpyzniMTTJWNg0SkSFo1fMc5/lzsBPrymKDbcszSOfKqSzE0Ykta0up8pNCPuSTA2VKTAc3naD1o84lO20Q4nmB0sMUi11qtm+CYx/HRe53vxi4t4t2co16HDS5MRHsHX+o6uXtDfFFNryprB6ko6AjqUu5zU7P4GpMxVw8Datc641E044=; user_pref_givenName=\"泽凯\"; user_pref_givenNameLocalLanguage=\"泽凯\"; alias=homepageproduct; auth_session=u1Ev_SG2IpaJz4CC.0sDeQPZosr95iAfsEcO8luoCzeaJ-ARdrUplaDcku8QDUstBSr4q8Jlzm-9ksQh7jHVVYshZ5qd_s5oSTgj3Uwj_ZM8uqi0nsXe52Uajo57_aJQ5442zRM3hEp4UbaysAkkG-vfd8l2f1DlC1fx_DjNpON7CaWJyH2w-2iXhDK-UGHo6gEh7yszzJn2k4Wc_M864wu3yqmRw8SOlcy5nNn1i78g2bh32vesc2a4H8vkvgDqxgWWQxOGg2r3_RSdXXwKRmDU6jKBv0YVaGHk5PS_1DDlGGcLEPY0HMTqnipMXr9svK0GJ_QcqiWlYw_i6Hdur01uRjLzIcdo294t1QnUMrcLIzN97p5FyaIwJG84aUkydyffA7PCYwjnBDMsWdRrFwRAoB9BuVxLyEKgguNyZMQb88QgKk8DAtYIinwBttlXA7ys-JJKq45F5Er-SLghbZ5P08oVRbaW0Ijh9lp36uwzrITqmawVBt49YlvcnFlMNlEZBfflUBiIBB1Ubz7gWArbveWSfwDcAFuKSHkimjBM1mDB7HNSPDg96QvnBII2fphyAmSgf-wZrp2LXuaVZcyFNEv-hVCA6hklJsx7eG07lBejeRFwc1GqA2McqblsrbjeULdmVxaH9EqCx2V7mjlbtuiR0ooKqGadhXNx_lGJINyplYe7q0pW-Kg__oYJqBb06iNFO3u9oJnH1-Y23kz-EEJdhUHRlf91I-9dc3vzOXfcoHbVeApyFL8GFELVz0W-vesEVfxt2O6neQUTegvA6Y_6bK33jMXFFCwS3AFrb3WjJwXYh_BTFHw52seEZPdx6uUc9PLcEMtT1AJsul3j4YVBPTtoCKXO1ek1rNqZwQYGbPsoaejYBNdFcasH1nz3vK4odnYeObq4Cxhq-mqcohymzaMQy-V5D_tJw2h58I2sjWKs_EcxSg8pf4uDdFlUywmYTNp17Yz5vc6sX48nztO2UfZ9QQZtN7EgYepZzoFc7SX1nNxfAWM6NN11oD9cxURWhct9HEz-n3nBI7bsZgZLHJVWclST1Ro5vg1rhdZp9rpTyQ_9Q-V62wxNCM5oFo6uFdCiv7rQJ6LqIUimuJbqRV2FgltF8cbKTxH7nwyJb4nHt9yX7CKn4jOiwdAodchdMxJZhtE3U6mIhcAOdXgcBBU8SkJY9bwbqgQLy7VdlzIFu9dr-6LoFa76lNUyCuJ3ba34FSOnzMjk-N21CgudxX8jg6IEQ-km14NVMs-YwVs-4IInnx8TWhbCIGDBNEQWl2rGTxLOzzHuXWKUaxeR1DEJOTfl1jxvyl3bwKn5yflM1fXMifIwXV4NZmJGTXqKvu5eHC8beQft-SV6mU-agwctoBYfJtX9xJdGOOd0ubI4QIFizJ-8rIY9daJIzI1gxeWQEE0h8bIqFsEUOViind2lf03cXOoHIlu8Jr3Rb0IYBn-BoRcn_YIz_m3KjpoQYt8hLITCzODguwEr_in0ZFaNx4pApifz1OrYZHA3FlS0As2a7ubO2wFdxQnQaZfFfG2G6NxzDjX5V5HzzL0Hcz3rn3uGiay-lC-LqPh0vGljzQuCuo7nYQkDK0vlXXKOOoN5Ea7XIkwGFos-TlQkDUo1ojSP8sbfhJTZ82ylsxHLMVfi0zgPRAnuU2QBXhEgacbpuQEkhf6TdAq3AhVlSBRilnrpCPmH98yE19T7QCPoawJMj5F0mbxu3Q_XrwpAZ0zJ-3KeDLD_SUbX_LIQe1MNZiYb_6EEeAFY4GLp31R8bNiyAhUR3hHsZSgg-VboZ3GBYiK3mFIp8QTQfuRGFN7l7JdhfE0BoewS6P9x0J3kXZ_eO4dgqg_S04kQJX4-sz2mPf5LrzKx2GuqVyydPCNUmQ2SX5uHGqZFeJZ3Kf8s.y7_Avxmch7v8ilj8DeROOg; ti_get_lit=ts=1639554461806|lit=bq76930|pid=|aid=|gpn=Non-Product|pt=search|evt=20211115154741806|; __ar_v4=G3YHLXUICZC3XKDXYVVLO4:20220013:5|2XNKMR6P4VGD5MD3ZP4SQR:20220013:5|QFXRHQEHOJDMLHSLFIWCLO:20220013:5; gpn=SN6501-gpn; tipage=/analog & mixed-signal/isolation/power for signal isolators/transformer drivers/sn6501/product folder-sn6501-cn; tipageshort=product folder-sn6501-cn; ticontent=/analog & mixed-signal/isolation/power for signal isolators/transformer drivers/sn6501; ga_content_cookie=/analog & mixed-signal/isolation/power for signal isolators/transformer drivers/sn6501; ga_page_cookie=product folder-sn6501-cn; ABTasty=uid=vypwvnt3c01qrsxa&fst=1639485229472&pst=1639485229472&cst=1639553042804&ns=2&pvt=21&pvis=18&th=686831.851794.6.4.2.1.1639485250594.1639553108637.1; ABTastySession=mrasn=&sen=26&lp=https%3A%2F%2Fwww.ti.com.cn%2Fsitesearch%2Fcn%2Fdocs%2Funiversalsearch.tsp%3FlangPref%3Dzh-CN%26searchTerm%3D%23src%3Dtop%26linkId%3D1%26q%3DBQ7693003DBT%26firstQueryCause%3DsearchFromLink; da_sid=5C752F108E33AE890EAFAA1349EA8210BE|3|0|3; da_lid=1D8F10B29A73EA12815BBB990B20DDA568|0|0|0; da_intState=0; bm_sz=042AE5E4EC4DE4DF1CD82F57F69CDBEE~YAAQdEInO7xFHaB9AQAAhtARvQ5soueigHq4QTN5RYB/VQ2x/8hZjIZb2idrUrmECOLB5J27ty4GlrStuaUo9uh9xDBM82R/RQfa0TQCpGUDLiDUWMNJiamxIDnWl8F4DEr9G2g2n7SZy5FtXIGuSeF/hwgeVeGVgW1xAdxh7gCddo7r3L9UnaK/9ZxjilGzVraBzGIdxLgdkPNhHQhQU+RvxmNkJb+qLimCLcbO3YuajGlR4gOOMwzkBEtATPn7rGYCxjl+HTzr9e9dH3Yu97iD7PRwBeb+PSyQb+ZmTX38FnznnZoEVim9Qlk3KQHalnjoelKjgJc2+3WBKJtdq+7vPiKZZptA5TLF7gigVFNL1dGo9c5bfO1zLetzAlf0cyhYmTRFHol8zY50bJM=~4272432~4535875; bm_sv=B29E08EDD7DA7D8E7E458A03597343DD~t/MxVMP+GCFj8jXhkZOnbOy75h5eb5vVdmqdBUlxDXUKRWFMnYVfMBjFDb6lUiO/1XY4WR1ahW4ozJn/cUZvKXQaTODK2pzcoe79y6hN+VBRWv70CR4WZKvaQ5Smd2jtA1I4HOIhWqTT8lRZh7rZLjzQqMyZUULHBbRq5Va9370=; ak_bmsc=DDE9C8652552C2CEACA8534EEBFF325C~000000000000000000000000000000~YAAQdEInO7FIHaB9AQAA/+IRvQ5t2ttScnBJ5+aXUAxdbC8uFvgoheNumHA4ks8SoPrz+x5tODU04yJ3uED3moqySkumk0uUprMIJGnjmsy0aDJXrcIfLj2Zg7VJsVBEs3SnWUF+ud6EIJmmPFF8F/4cwy3uJheEbdayBsp81VEJO6ffNTNH6tctqimUShFAIkj+TCFo7vbd89vzJvNMw+IePKRNjBsqOz3qJXIBQGtlFuXA0ingtJXjstxpYzZvEh/noF07UiykqTuIvN34T74GBpAq8CpLqRbYgmeGRRct9aqhugiVfDdvw1CrjMJS2Dd1kl+wQwKhHeA/RTt18seZp3lISsBjMcuwZ3gEHIyPqpKKEHSE79C9QFRXSXgzaeemdIxW1EplMApaNC9MprTj9GKqSlde8JB2vFPn0L8QR7bYNZgvCE+QwNYPDiy2gEgoMcz9CagRiF58HWpcpXQjZ2pcLGdmUS5X1HbrYKrN77DzoUXaLgaCHlUXHfKbGg==; userType=Anonymous; _gat_ga_main_tracker=1; utag_main=free_trial:false$_st:1639556928648$v_id:017db8ef47bc007120575ddec7a805071005206900bd0$_sn:2$_ss:0$dc_visit:2$_pn:18;exp-session$ses_id:1639553042110;exp-session$dc_event:25;exp-session$dc_region:ap-east-1;exp-session; ti_rid=13264382; _abck=96110CECF7C7315B77F0E0EF27EAB96A~-1~YAAQdEInOzlMHqB9AQAAg80ZvQcPKfsCsm8G1p1EyG8n+XPT6Q/rGTTZm5afGsL1wjwdBk7lOXXz4INhkB2CJ4SsQDCRKcNw4O2ADmOmlI1YuvNV2+GlevTyI8/T+F8WyoVyCswxvb8cw5nvp0E8xNIutNYwdFKxU4ka696Kq8ICMs1I9k0rWPvtRx9LxGMK7wNxQJxCJ8YuD9DwV9pL36/Z3NfZrFBeS1CAQlGsDeneIDf2j79KSDpYR/PvJnHPYzU3OtwNigh78gXNwzYSNmrUeoz1oP0hRjXYyvplC77qh55lg4zm+FTqDU0GcrtMUAV5KaF6hlvSuvKu1yRnmKkKJqKwfyevRaE7LRNLBiv0xXVkllxbR+jpecSFvVEdZHmlWcCR+IUfbVx7wZRliNZH1sKDM9qATQVrlkdg9ODbI/F7oJ82PgVgzuAnKpVOLoSJVNGu1U/kZqCL/UNqIhilXV+mIE3Jll0sWatutYk=~-1~-1~-1");
    		httpConn.setRequestProperty("authority", "www.ti.com.cn");
    		httpConn.setRequestProperty("method", "POST");
    		httpConn.setRequestProperty("path", "/occservices/v2/ti/addtocart");
    		httpConn.setRequestProperty("scheme", "https");
    		httpConn.setRequestProperty("accept", "*/*");
    		httpConn.setRequestProperty("accept-encoding", "gzip, deflate, br");
    		httpConn.setRequestProperty("accept-language", "zh-CN,zh;q=0.9");
    		httpConn.setRequestProperty("cache-control", "no-store, must-revalidate");
    		httpConn.setRequestProperty("content-length", "214");
    		httpConn.setRequestProperty("expires", "0");
    		httpConn.setRequestProperty("newrelic", "eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjE3MjA1OTQiLCJhcCI6IjExODYyMDk5ODQiLCJpZCI6IjcxMTlhMWYyZmY4MWIyMTQiLCJ0ciI6ImVhZjBmOWQ1Y2FmZmQwYWY1YTYzZWVlZGY5MzJiZDYwIiwidGkiOjE2Mzk1NTUxMjg3NTYsInRrIjoiMTU2NTEzNiJ9fQ==");
    		httpConn.setRequestProperty("origin", "https://www.ti.com.cn");
    		httpConn.setRequestProperty("referer", "https://www.ti.com.cn/product/cn/SN6501");
    		httpConn.setRequestProperty("sec-ch-ua", "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"");
    		httpConn.setRequestProperty("sec-ch-ua-mobile", "?0");
    		httpConn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
    		httpConn.setRequestProperty("sec-fetch-dest", "empty");
    		httpConn.setRequestProperty("sec-fetch-mode", "cors");
    		httpConn.setRequestProperty("sec-fetch-site", "same-origin");
    		httpConn.setRequestProperty("traceparent", "00-eaf0f9d5caffd0af5a63eeedf932bd60-7119a1f2ff81b214-01");
    		httpConn.setRequestProperty("tracestate", "1565136@nr=0-1-1720594-1186209984-7119a1f2ff81b214----1639555128756");
    		httpConn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4662.6 Safari/537.36");
    		httpConn.setRequestProperty("x-sec-clge-req-type", "ajax");
            //
            
            // 设置POST方式  
            httpConn.setDoInput(true);  
            httpConn.setDoOutput(true);  
            // 获取HttpURLConnection对象对应的输出流  
            out = new PrintWriter(httpConn.getOutputStream());
            // 发送请求参数  
            out.write(params);  
            // flush输出流的缓冲  
            out.flush();  
            // 定义BufferedReader输入流来读取URL的响应，设置编码方式  
            in = new BufferedReader(new InputStreamReader(httpConn
                    .getInputStream(), "UTF-8"));  
            String line;
            // 读取返回的内容  
            while ((line = in.readLine()) != null) {  
                result += line;  
            }  
        } catch (Exception e) {
            e.printStackTrace();  
        } finally {  
            try {  
                if (out != null) {  
                    out.close();  
                }  
                if (in != null) {  
                    in.close();  
                }  
            } catch (IOException ex) {
                ex.printStackTrace();  
            }  
        }  
        //System.out.println("返回结果是:"+result);
        return result;  
    }  

}
