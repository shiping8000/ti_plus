package com.qiangou.ti.vo;

public class Cart {
	
	
	// TLC5916ID
	
	//"packageOption": "CTX",
	private String packageOption;
    //"opnId": "V62/05603-01XE",  产品型号
	private String opnId;
    //"quantity": "2",  数量
	private String quantity;  
    //"tiAddtoCartSource": "ti.com-productfolder",  
	private String tiAddtoCartSource;
    //"sparam": ""
    private String sparam;
	public String getPackageOption() {
		return packageOption;
	}
	public void setPackageOption(String packageOption) {
		this.packageOption = packageOption;
	}
	public String getOpnId() {
		return opnId;
	}
	public void setOpnId(String opnId) {
		this.opnId = opnId;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTiAddtoCartSource() {
		return tiAddtoCartSource;
	}
	public void setTiAddtoCartSource(String tiAddtoCartSource) {
		this.tiAddtoCartSource = tiAddtoCartSource;
	}
	public String getSparam() {
		return sparam;
	}
	public void setSparam(String sparam) {
		this.sparam = sparam;
	}
	

}
