package com.qiangou.ti.vo;

public class CartVo2 {
	
	
	private String packageOption; //": null,
    private String opnId;//": "BQ7693003DBT",
    private String quantity;//": "1",
    private String tiAddtoCartSource; //": "ti.com-opnsearch",
    private String dienCode; //": "",
    private String year; //": "",
    private String week; //": "",
    private String batchCode;  //": "",
    private String pcrCode; //": "",
    private String sparam;  //": ""
	public String getPackageOption() {
		return packageOption;
	}
	public void setPackageOption(String packageOption) {
		this.packageOption = packageOption;
	}
	public String getOpnId() {
		return opnId;
	}
	public void setOpnId(String opnId) {
		this.opnId = opnId;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTiAddtoCartSource() {
		return tiAddtoCartSource;
	}
	public void setTiAddtoCartSource(String tiAddtoCartSource) {
		this.tiAddtoCartSource = tiAddtoCartSource;
	}
	public String getDienCode() {
		return dienCode;
	}
	public void setDienCode(String dienCode) {
		this.dienCode = dienCode;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getPcrCode() {
		return pcrCode;
	}
	public void setPcrCode(String pcrCode) {
		this.pcrCode = pcrCode;
	}
	public String getSparam() {
		return sparam;
	}
	public void setSparam(String sparam) {
		this.sparam = sparam;
	}

    
    
    
}
