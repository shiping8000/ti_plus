package com.qiangou.ti.vo;

public class CertVo {
	
	
	
	private String batchCode;
	private String 	dienCode; 		//: ""
	private String 	opnId;    		//: "TAS5558DCA"
	private String 	packageOption;  //: null
	private String 	pcrCode; 		//: ""
	private String 	quantity; //: "6"
	private String 	sparam;    //: ""
	private String 	tiAddtoCartSource;  //: "ti.com-productfolder"
	private String 	week;  	//: ""
	private String 	year; 	//: ""
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getDienCode() {
		return dienCode;
	}
	public void setDienCode(String dienCode) {
		this.dienCode = dienCode;
	}
	public String getOpnId() {
		return opnId;
	}
	public void setOpnId(String opnId) {
		this.opnId = opnId;
	}
	public String getPackageOption() {
		return packageOption;
	}
	public void setPackageOption(String packageOption) {
		this.packageOption = packageOption;
	}
	public String getPcrCode() {
		return pcrCode;
	}
	public void setPcrCode(String pcrCode) {
		this.pcrCode = pcrCode;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getSparam() {
		return sparam;
	}
	public void setSparam(String sparam) {
		this.sparam = sparam;
	}
	public String getTiAddtoCartSource() {
		return tiAddtoCartSource;
	}
	public void setTiAddtoCartSource(String tiAddtoCartSource) {
		this.tiAddtoCartSource = tiAddtoCartSource;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	

}
